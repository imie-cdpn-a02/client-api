[![build status](https://gitlab.com/imie-cdpn-a02/client-api/badges/master/build.svg)](https://gitlab.com/imie-cdpn-a02/client-api/commits/master)

# Client API

API de gestion des clients

# Endpoints

L'application a deux endpoints :
* POST /signup : créé un nouvel client
* POST /authenticate : authentifie un client
* GET /clients/:client_reference : renvoie les informations d'un client

## Créer un client

Exemple de requête POST /signup :
```json
{
  "email": "client@example.com",
  "password": "password",
  "phone": "+330123456789",
  "address": "123 rue machin, 49000 Angers"
}
```

Success : la requête renvoie un statut 201 et la référence du client
```json
{
  "reference": "C-AZE12",
}
```

Error : la requête renvoie un statut 409 et un message d'erreur.
```json
{
  "error": "Cet email est déjà utilisé",
}
```

## Authentifier un client

Exemple de requête POST /authenticate :
```json
{
  "email": "client@example.com",
  "password": "password",
}
```

Success : la requête renvoie un statut 200 et la référence du client
```json
{
  "reference": "C-AZE12",
}
```

Error : la requête renvoie un statut 4xx.

## Obtenir les informations d'un client

Requête GET /clients/:client_reference :

Success : la requête renvoie un statut 200 et les informations du client :
```json
{
  "email": "client@example.com",
  "phone": "+330123456789",
  "address": "123 rue machin, 49000 Angers"
}
```

Error : la requête renvoie un statut 404.
