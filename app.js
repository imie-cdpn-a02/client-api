'use strict';

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

// DB
const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://mongo/client_api_dev';
mongoose.Promise = global.Promise;
mongoose.connect(MONGODB_URI, { useMongoClient: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("we're connected!");
});

// Constants
const PORT = process.env.PORT || 3000;
const HOST = process.env.HOST || '0.0.0.0';

// Routes
const routes = require('./routes');

// App
const app = express();
app.use(bodyParser.json());
app.use(routes);

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

module.exports = app;
