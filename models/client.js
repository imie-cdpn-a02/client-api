const bcrypt = require('bcrypt');
const mongoose = require("mongoose");

const SALT_FACTOR = 10;

const clientSchema = mongoose.Schema({
  email: { type: String, required: true, unique: true },
  reference: { type: String, unique: true },
  password: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
  address: { type: String, required: true },
  phone: { type: String, required: true },
});

function generateClientReference() {
  return 'C-' + Math.random().toString(36).substring(2,7).toUpperCase();
}

clientSchema.pre("save", function(done) {
  const client = this;
  if (!client.isModified("password")) {
    return done();
  }
  bcrypt.hash(client.password, SALT_FACTOR, function(err, hashedPassword) {
    if (err) { return done(err); }

    client.password = hashedPassword;
    client.reference = generateClientReference();
    done();
  });
});

clientSchema.methods.checkPassword = function(guess, done) {
  bcrypt.compare(guess, this.password, function(err, isMatch) {
    done(err, isMatch);
  });
};

const Client = mongoose.model("Client", clientSchema);
module.exports = Client;
