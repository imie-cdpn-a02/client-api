const express = require('express');
const router = express.Router();
const Client = require('./models/client');
const Services = require('./services');

router.post('/signup', (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const phone = req.body.phone;
  const address = req.body.address;
  Client.findOne({ email: email }, function(err, client) {
    if (err) { return next(err); }

    if (client) {
      return res
        .status(409)
        .send({ 'error': 'Cet email est déjà utilisé' });
    }

    let newClient = new Client({
      email: email,
      password: password,
      phone: phone,
      address: address
    });
    newClient.save(() => {
      req.clientReference = newClient.reference;
      Services.notifyCreate(newClient);
      next();
    });
  });
}, (req, res) => {
  res
    .status(201)
    .send({ 'reference': req.clientReference });
});

router.post('/authenticate', (req, res, next) => {
  let email = req.body.email;
  let password = req.body.password;

  Client.findOne({ email: email }, function(err, client) {
    if (client) {
      client.checkPassword(password, function(err, isMatch) {
        if (err) { return done(err); }

        if (isMatch) {
          return res.send({ reference: client.reference });
        }
        console.log('FAILED');

        return res.sendStatus(404);
      });
    } else {
      return res.sendStatus(404);
    }
  });
});

router.get('/clients/:reference', function(req, res) {
  let reference = req.params.reference;
  Client.findOne({ reference: reference }, function(err, client) {
    if (client) {
      res.send(
        {
          email: client.email,
          phone: client.phone,
          address: client.address,
        }
      );
    } else {
      res.sendStatus(404);
    }
  });
});

module.exports = router;
