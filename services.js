const request = require('request');

let Services = function() {};
const notification_api_url = process.env.NOTIFICATION_API_URL;
const email_url = `${notification_api_url}/orderConfirmation.php`;
request.debug = true

Services.prototype.notifyCreate = function(client, callback) {
  let params =
    {
      userRef: client.reference,
      subject: "Merci d'avoir choisi FTGO !",
      corps: 'Bonjour,\nVotre compte a bien été créé.'
    }
  request.post(email_url, params, callback);
};

module.exports = new Services();
