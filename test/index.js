process.env.MONGODB_URI = 'mongodb://mongo/client_api_test'
const app = require('../app.js');
const supertest = require('supertest');
const Services = require('../services');
const sinon = require('sinon');
const expect = require('chai').expect;
const Client = require('../models/client');
var mongoose = require('mongoose');

let services;

before(function() {
  services = sinon.stub(Services, 'notifyCreate');
});

beforeEach(function(done) {
  Client.remove(done);
});

const email = 'client@example.com';
const validParams = {
  email: email,
  password: 'password',
  phone: '+330123456789',
  address: '123 rue machin, 49000 Angers'
};

describe('POST signup', function() {
  context('success', function() {
    it('creates a client and return client reference', function(done) {
      supertest(app)
        .post('/signup')
        .send(validParams)
        .expect(201)
        .end(function(err, res) {
          Client.findOne({ 'email': email }, function(err, client) {
            expect(client).to.not.be.null;
            expect(res.body).to.have.property('reference');
            expect(res.body.reference).to.equal(client.reference);
            done();
          });
        });
    });

    it('notifies the client', function(done) {
      supertest(app)
        .post('/signup')
        .send(validParams)
        .end(function(err, res) {
          expect(services.called).to.be.true;
          done();
        });
    });
  });

  context('client email already exists', function() {
    beforeEach(function(done) {
      supertest(app)
        .post('/signup')
        .send(validParams)
        .end(done);
    });

    it('returns and error if client email already exists', function(done) {
      supertest(app)
        .post('/signup')
        .send(validParams)
        .expect(409)
        .end(function(err, res) {
          expect(res.body.error).to.equal('Cet email est déjà utilisé');
          done();
        });
    });
  });
});

describe('POST authenticate', function() {
  const params = {
    email: email,
    password: 'password'
  };

  beforeEach(function(done) {
    new Client(validParams).save(done);
  });

  context('success', function() {
    it('returns client reference', function(done) {
      supertest(app)
        .post('/authenticate')
        .send(params)
        .expect(200)
        .end(function(err, res) {
          Client.findOne({ 'email': email }, function(err, client) {
            expect(res.body).to.have.property('reference');
            expect(res.body.reference).to.equal(client.reference);
            done();
          });
        });
    });
  });

  context('invalid password', function() {
    it('returns 404', function(done) {
      supertest(app)
        .post('/authenticate')
        .send({ email: email, password: 'aze' })
        .expect(404, done);
    });
  });

  context('invalid email', function() {
    it('returns 404', function(done) {
      supertest(app)
        .post('/authenticate')
        .send({ email: 'aze@qsd' })
        .expect(404, done);
    });
  });
});

describe('GET /clients/:reference', function() {
  context('client exists', function() {
    let clientReference;

    beforeEach(function(done) {
      let client = new Client(validParams);
      client.save(function() {
        clientReference = client.reference;
        done();
      });
    });

    it('returns client information', function(done) {
      supertest(app)
        .get(`/clients/${clientReference}`)
        .expect(200)
        .end(function(err, res) {
          expect(res.body.email).to.equal(validParams.email);
          expect(res.body.phone).to.equal(validParams.phone);
          expect(res.body.address).to.equal(validParams.address);
          done();
        });
    });

    context('client does not exist', function() {
      it('returns client information', function(done) {
        supertest(app)
          .get('/clients/AZE123')
          .expect(404, done);
      });
    });
  });
});
